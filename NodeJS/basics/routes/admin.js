const express = require('express');

const router = express.Router();

router.post('/product', (req, res, next) => {
    console.log(req.body);
    res.redirect('/');
})

router.get('/add-product', (req, res, next) => {
    console.log("First");
    res.send(`<h1>Product Added</h1><form method="POST" action="/admin/product"><input type="text" name="title"><button type="submit">Click</button></form>`);
    // res.redirect('/');
})

module.exports = router;