const express = require('express');
const path = require('path');

const rootDir = require('../utils/path');

const router = express.Router();

router.get('/', (req, res, next) => {
    console.log("Second");
    console.log(path.join(rootDir));
    res.send("<h1>Hello</h1>");
})

module.exports = router;