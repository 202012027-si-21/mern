import express, {Application, Request, Response, NextFunction} from "express";
import {route as adminRoutes} from "./routes/admin"

const app:Application = express();

app.use(adminRoutes)

const PORT: number = 5000;
app.listen(PORT, () => console.log("Server running at port " + PORT + "."))