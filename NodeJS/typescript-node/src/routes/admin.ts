import express, {Router, Request, Response, NextFunction} from "express"

const route: Router = express.Router();

route.get('/', (req:Request, res:Response) => {
    res.send("Hello coders!")
})

export {route}