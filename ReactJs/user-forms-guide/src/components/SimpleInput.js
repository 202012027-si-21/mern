import { useState } from "react";

const SimpleInput = (props) => {
    const [nameInput, setNameInput] = useState("");
    const nameInputChangeHandler = (event) => {
        setNameInput(event.target.value);
    };

    const submitHandler = (event) => {
        event.preventDefault();

        if (nameInput.trim() === "") {
            return;
        }
        console.log(nameInput);
    };

    return (
        <form onSubmit={submitHandler}>
            <div className="form-control">
                <label htmlFor="name">Your Name</label>
                <input
                    type="text"
                    id="name"
                    onChange={nameInputChangeHandler}
                />
            </div>
            <div className="form-actions">
                <button>Submit</button>
            </div>
        </form>
    );
};

export default SimpleInput;
