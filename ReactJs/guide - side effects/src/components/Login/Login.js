import React, { useState, useEffect, useReducer } from 'react';

import Card from '../UI/Card/Card';
import classes from './Login.module.css';
import Button from '../UI/Button/Button';

const emailReducer = (state, action) => {
  if(action.type === "USER_INPUT"){
    return {value: action.val, isValid: action.val.includes('@')}
  }
  if(action.type === "INPUT_BLUR"){
    return {value: state.value, isValid: state.value.includes('@')}
  }

  return {value: '', isValid: false}
}

const passwdReducer = (state, action) => {
  if(action.type === "USER_INPUT"){
    return {value: action.val, isValid: action.val.trim().length > 6}
  }
  if(action.type === "INPUT_BLUR"){
    return {value: state.value, isValid: state.value.trim().length > 6}
  }
  return {value: '', isValid: false}
}


const Login = (props) => {
  const [formIsValid, setFormIsValid] = useState(false);

  const [emailState, emailDispatch] = useReducer(emailReducer, {
    value: '',
    isValid: null
  })

  const [passwdState, passwdDispatch] = useReducer(passwdReducer, {
    value: '',
    isValid: null
  })

  useEffect(() => {
    const identifier = setTimeout(() => {
      console.log("Validating!")
      setFormIsValid(
        emailState.value.includes('@') && passwdState.value.trim().length > 6
      );
    }, 500)

    return () => {
      clearTimeout(identifier)
    }   
  }, [emailState, passwdState]);

  const emailChangeHandler = (event) => {
    emailDispatch({type: "USER_INPUT", val: event.target.value});

    // setFormIsValid(
    //   event.target.value.includes('@') && passwdState.value.trim().length > 6
    // );
  };

  const passwordChangeHandler = (event) => {
    passwdDispatch({type: "USER_INPUT", val: event.target.value});

    // setFormIsValid(
    //   emailState.value.includes('@') && event.target.value.trim().length > 6
    // );
  };

  const validateEmailHandler = () => {
    emailDispatch({type: "INPUT_BLUR"});
  };

  const validatePasswordHandler = () => {
    passwdDispatch({type: "INPUT_BLUR"});
  };

  const submitHandler = (event) => {
    event.preventDefault();
    props.onLogin(emailState.value, passwdState.value);
  };

  return (
    <Card className={classes.login}>
      <form onSubmit={submitHandler}>
        <div
          className={`${classes.control} ${
            emailState.isValid === false ? classes.invalid : ''
          }`}
        >
          <label htmlFor="email">E-Mail</label>
          <input
            type="email"
            id="email"
            value={emailState.value}
            onChange={emailChangeHandler}
            onBlur={validateEmailHandler}
          />
        </div>
        <div
          className={`${classes.control} ${
            passwdState.isValid === false ? classes.invalid : ''
          }`}
        >
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={passwdState.value}
            onChange={passwordChangeHandler}
            onBlur={validatePasswordHandler}
          />
        </div>
        <div className={classes.actions}>
          <Button type="submit" className={classes.btn} disabled={!formIsValid}>
            Login
          </Button>
        </div>
      </form>
    </Card>
  );
};

export default Login;
