import React, {useState, useEffect} from "react";

const AuthContext = React.createContext({
    isLoggedIn: false,
    loginHandler: () => {},
    logoutHandler: () => {}
})

export const AuthContextProvider = props => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
      var logInStatus = localStorage.getItem('loggedIn')
      if(logInStatus === '1'){
        setIsLoggedIn(true)
      }
  
    }, [])
  
    const loginHandler = (email, password) => {
      setIsLoggedIn(true);
      localStorage.setItem('loggedIn', '1')
    };
  
    const logoutHandler = () => {
      setIsLoggedIn(false);
      localStorage.setItem('loggedIn', '0')
    };


   return ( <AuthContext.Provider
    value={{isLoggedIn: isLoggedIn,
        loginHandler: loginHandler,
        logoutHandler: logoutHandler}}>
        {props.children}
    </AuthContext.Provider>)
}


export default AuthContext