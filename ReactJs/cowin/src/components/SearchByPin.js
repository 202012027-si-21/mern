import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  MDBRow, MDBCol,
} from 'mdb-react-ui-kit'
import { Form, Button } from 'react-bootstrap';
import { initiateSearch } from '../actions/availabilityActions';

const SearchByPin = ({ searchParams }) => {
  const [pinCodeInput, setPinCodeInput] = useState(searchParams.pincode !== undefined ? searchParams.pincode : '');
  const [pinCodeIsInValid, setPinCodeIsInValid] = useState(false);
  const [pinCodeIsTouched, setPinCodeIsTouched] = useState(false);

  const dispatch = useDispatch();

  const validatePinCode = (e) => {
    if (e.target.value.trim().length > 0) {
      setPinCodeIsInValid(false)
    } else {
      setPinCodeIsInValid(true)
    }
    setPinCodeInput(e.target.value.replace(/[^0-9]/g, ''))
  }

  const submitHandler = (e) => {
    e.preventDefault()
    if (pinCodeInput.trim().length > 0) {
      setPinCodeIsInValid(false)
      dispatch(initiateSearch('pincode', { pincode: pinCodeInput }))
      // dispatch(initiateSearch('district', { district_id: 395 }))
    } else {
      setPinCodeIsInValid(true)
      setPinCodeIsTouched(true)
    }
  }

  return (
    <Form onSubmit={submitHandler} className='search__pin'>
      <MDBRow>
        <MDBCol sm={12} md={9}>
          <Form.Group>
            <Form.Control className='rounded-pill py-2 px-4' placeholder='Enter your PIN' type="text" value={pinCodeInput} onChange={validatePinCode} onBlur={validatePinCode} onFocus={() => setPinCodeIsTouched(true)} isInvalid={pinCodeIsTouched && pinCodeIsInValid} maxLength={6} />
            <Form.Control.Feedback type="invalid" className='input__error'>
              Please enter correct pincode
            </Form.Control.Feedback>
          </Form.Group>
        </MDBCol>
        <MDBCol sm={12} md={3}>
          <Button variant='primary' type='submit' className='btn-rounded searchBtn w-100 w-md-75 mt-2 mt-md-0' >Search</Button>
        </MDBCol>
      </MDBRow>
    </Form>
  )
}

export default SearchByPin
