import React from 'react';
import logo from '../assets/images/emblem-gov.png';
import { Image } from 'react-bootstrap';
import { MDBNavbar, MDBContainer, MDBNavbarNav, MDBNavbarBrand, MDBNavbarLink } from 'mdb-react-ui-kit';
// import { Link } from 'react-router-dom';
// import { LinkContainer } from 'react-router-bootstrap';

const Header = () => {
  return (
    <MDBContainer className='secondaryHeader px-0' fluid>
      <MDBNavbar bgColor='primary' dark expand='md'>
        <MDBContainer breakpoint='md' className='px-3 px-md-0'>
          <MDBNavbarBrand href='https://www.india.gov.in/' rel='noreferrer' target='_blank' className='secondaryHeader-brand py-0'>
            <span>
              <Image
                alt=''
                src={logo}
                className='align-top secondaryHeader-brand__img'
              />
            </span>
            <span className='text-wrap fw-300 secondaryHeader-brand__title'>
              Ministry of Health and Family Welfare
            </span>
          </MDBNavbarBrand>


          <MDBNavbarNav className='ms-auto me-2 me-md-0 py-1 py-md-0 d-flex w-auto'>
            <MDBNavbarLink className='border-end py-0' href='https://dashboard.cowin.gov.in/' target='_blank' rel='noreferrer'>Dashboard</MDBNavbarLink>
            <MDBNavbarLink href='/' className='py-0'>Skip To Main Content</MDBNavbarLink>
          </MDBNavbarNav>
        </MDBContainer>
      </MDBNavbar>
    </MDBContainer>
  );
};

export default Header;
