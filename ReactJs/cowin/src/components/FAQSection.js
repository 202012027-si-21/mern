import { MDBContainer, MDBAccordion, MDBAccordionItem, MDBRow, MDBCol } from 'mdb-react-ui-kit'
import { QuestionCircle } from 'react-bootstrap-icons'
import React from 'react'

const FAQSection = () => {
  return (
    <MDBContainer breakpoint='xl' id='faqSection' className='my-5'>
      <h1 className='text-center mb-5'>Frequently Asked Questions</h1>
      <div className='faq-container'>
        <div className='card-title'>
          <h3>Top Questions</h3>
        </div>
        <div className='faq-accordian'>
          <MDBAccordion alwaysOpen initialActive='flush-collapse1'>
            <MDBAccordionItem collapseId='flush-collapse1' headerTitle='Is online registration mandatory for Covid 19 vaccination?'>
              Vaccination Centres provide for a limited number of on-spot registration slots every day. Beneficiaries aged 45 years and above can schedule appointments online or walk-in to vaccination centres. Beneficiaries aged 18 years and above can schedule appointments online or walk-in to Government vaccination centres. However, beneficiaries aged 18-44 years should mandatorily register themselves and schedule appointment online before going to a Private vaccination centre.
              <br /><br />
              In general, all beneficiaries are recommended to register online and schedule vaccination in advance for a hassle-free vaccination experience.
            </MDBAccordionItem>

            <MDBAccordionItem collapseId='flush-collapse2' headerTitle='How many people can be registered in the CoWIN portal through one mobile number?'>
              Up to 4 people can be registered for vaccination using the same mobile number.
            </MDBAccordionItem>

            <MDBAccordionItem collapseId='flush-collapse3' headerTitle='Can I register for vaccination without Aadhaar card?'>
              Yes, you can register on CoWIN portal using any of the following ID proofs:<br />
              a. Aadhaar card<br />
              b. Driving License<br />
              c. PAN card<br />
              d. Passport<br />
              e. Pension Passbook<br />
              f. NPR Smart Card<br />
              g. Voter ID (EPIC)<br />
              h. Unique Disability ID (UDID)<br />
              i. Ration Card<br />
            </MDBAccordionItem>

            <MDBAccordionItem collapseId='flush-collapse4' headerTitle='Is there any registration charges to be paid?'>
              No. There is no registration charge.
            </MDBAccordionItem>

            <MDBAccordionItem collapseId='flush-collapse5' headerTitle='Can I check the vaccine being administered at each vaccination centre?'>
              Yes, while scheduling an appointment for vaccination, the system will show vaccination centre names along with the name of the vaccine that will be administered.
            </MDBAccordionItem>

            <MDBAccordionItem collapseId='flush-collapse6' headerTitle='The appointment can be rescheduled at any time. In case you are not able to go for vaccination on the date of appointment, you can reschedule the appointment by clicking on “Reschedule” tab.'>
              The appointment can be rescheduled at any time. In case you are not able to go for vaccination on the date of appointment, you can reschedule the appointment by clicking on “Reschedule” tab.
            </MDBAccordionItem>

            <MDBAccordionItem collapseId='flush-collapse7' headerTitle='Where will I receive confirmation of date and time of vaccination?'>
              Once an appointment is scheduled, you will receive the details of the vaccination centre, date and time slot chosen for appointment in an SMS sent to your registered mobile number. You can also download the appointment slip and print it or keep it on your smart phone.
            </MDBAccordionItem>

            <MDBAccordionItem collapseId='flush-collapse8' headerTitle="When I click on vaccination centre it shows ' No appointments are available in this period'. What to do?">
              In case of no availability of slots for scheduling appointment for vaccination in the searched vaccination centre, you may try scheduling appointment in other nearby centres. The portal gives you the feature of searching vaccination centres using your PIN code and District.
            </MDBAccordionItem>


          </MDBAccordion>
        </div>
      </div>

      <MDBRow className='mt-4'>
        <MDBCol sm={12} md={12} className='text-center text-md-end'>
          <a className='btn btn-primary rounded-pill px-4' href="https://www.cowin.gov.in/faq" target='_blank' rel='noreferrer' style={{ fontSize: '0.875rem' }}><QuestionCircle width={16} height={16} style={{ marginRight: '0.4rem' }}></QuestionCircle> READ ALL F.A.Q.</a>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  )
}

export default FAQSection
