import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import { ArrowRight } from 'react-bootstrap-icons'
import React from 'react'

const ContactUsSection = () => {
  return (
    <MDBContainer breakpoint='xl' id='contactUsSection'>
      <h1 className='text-center mb-5'>How Can We Help You?</h1>
      <MDBRow>
        <MDBCol sm={12} md={6}>
          <h2>Raise An Issue</h2>
          <ul className='helpListUL'>
            <li>
              <a href="https://selfregistration.cowin.gov.in/selfregistrationcertificate-correction" target='_blank' rel='noreferrer'>
                <span>Get Your Certificate Corrected</span>
                <ArrowRight width={16} height={16}></ArrowRight>
              </a>
            </li>

            <li>
              <a href="https://selfregistration.cowin.gov.in/selfregistrationcertificate-correction"
                target='_blank' rel='noreferrer'>
                <span>Add Passport Details in vaccination certificate</span>
                <ArrowRight width={16} height={16}></ArrowRight>
              </a>
            </li>

            <li>
              <a href="https://selfregistration.cowin.gov.in/selfregistrationcertificate-correction" target='_blank' rel='noreferrer'>
                <span>vaccine certificate not received</span>
                <ArrowRight width={16} height={16}></ArrowRight>
              </a>
            </li>

            <li>
              <a href="https://selfregistration.cowin.gov.in/selfregistrationcertificate-correction" target='_blank' rel='noreferrer'>
                <span>merge multiple dose#1 certificate</span>
                <ArrowRight width={16} height={16}></ArrowRight>
              </a>
            </li>
          </ul>
        </MDBCol>

        <MDBCol sm={12} md={1}>
        </MDBCol>

        <MDBCol className='mt-3 mt-md-0' sm={12} md={5}>
          <h2 className='contact-us__title'>Contact Us</h2>
          <MDBRow className='contact-us__details mb-2'>
            <MDBCol sm={12} md={4}>
              <h5>Helpline</h5>
            </MDBCol>
            <MDBCol sm={12} md={8}>
              +<a href='tel:911123978046'>91 11 23978046</a> (Toll Free - <a href='tel:1075'>1075</a>)
            </MDBCol>
          </MDBRow>

          <MDBRow className='contact-us__details mb-2'>
            <MDBCol sm={12} md={4}>
              <h5>Technical Helpline</h5>
            </MDBCol>
            <MDBCol sm={12} md={8}>
              <a href='tel:01204473222'>0120 4473222</a>
            </MDBCol>
          </MDBRow>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  )
}

export default ContactUsSection
