import React, { useState, useEffect } from 'react'
import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import { Button, Form } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { getStatesList, getDistrictsList } from '../actions/metadataActions'
import { initiateSearch } from '../actions/availabilityActions';

import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const SearchByDistrict = ({ searchParams }) => {
  const { states, districts } = useSelector(state => state.metadata)

  const [stateInput, setStateInput] = useState(0);
  const [stateIsInValid, setStateIsInValid] = useState(false);
  const [stateIsTouched, setStateIsTouched] = useState(false);

  const [districtInput, setDistrictInput] = useState(0);
  const [districtIsInValid, setDistrictIsInValid] = useState(false);
  const [districtIsTouched, setDistrictIsTouched] = useState(false);

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getStatesList())
  }, [dispatch])

  useEffect(() => {
    if (states.length > 0 && searchParams.state_id !== undefined) {
      setStateInput(searchParams.state_id)
      if (districts.length === 0) {
        console.log("State changed")
        dispatch(getDistrictsList(searchParams.state_id))
      }
    }

    if (districts.length > 0 && searchParams.district_id !== undefined) {
      const d = districts.find(ele => ele.district_id === searchParams.district_id)
      if (d) {
        setDistrictInput(searchParams.district_id)
      } else {
        setDistrictInput(0)
      }
    }
  }, [states, districts, searchParams, dispatch])

  const validateState = (e) => {
    if (e.target.value > 0) {
      setStateIsInValid(false)
    } else {
      setStateIsInValid(true)
    }
    setStateInput(e.target.value)
  }

  const changeState = (e) => {
    validateState(e)
    setDistrictInput(0)
    if (!stateIsInValid) {
      dispatch(getDistrictsList(e.target.value))
    }
  }

  const validateDistrict = (e) => {
    if (e.target.value > 0) {
      setDistrictIsInValid(false)
    } else {
      setDistrictIsInValid(true)
    }
    setDistrictInput(e.target.value)
  }

  const submitHandler = (e) => {
    e.preventDefault()
    if (stateInput > 0 && districtInput > 0) {
      setStateIsInValid(false)
      setDistrictIsInValid(false)
      // console.log(stateInput)
      // console.log(districtInput)
      // dispatch(initiateSearch('pincode', { pincode: pinCodeInput }))
      dispatch(initiateSearch('district', { state_id: stateInput, district_id: districtInput }))
    } else {
      if (!stateInput > 0) {
        setStateIsInValid(true)
        setStateIsTouched(true)
      }
      if (!districtInput > 0) {
        setDistrictIsInValid(true)
        setDistrictIsTouched(true)
      }
    }
  }

  return (
    <Form onSubmit={submitHandler} className='search__district'>
      <MDBRow>
        <MDBCol sm={12} md={5} className='mt-2 mt-md-0'>
          <FormControl className='form-control__district'>
            <Select
              displayEmpty
              className='rounded-pill border'
              value={stateInput}
              onChange={changeState}
              onBlur={validateState}
              onFocus={() => setStateIsTouched(true)}
            >
              <MenuItem value="0" readOnly={true} hidden={true} className='form-control__placeholder'>
                <em>Select State</em>
              </MenuItem>
              {states.map(state => {
                return <MenuItem key={state.state_id} value={state.state_id} className='select-option'>{state.state_name}</MenuItem>
              })}
            </Select>
            {stateIsTouched && stateIsInValid && <FormHelperText>Please select correct state</FormHelperText>}
          </FormControl>
        </MDBCol>

        <MDBCol sm={12} md={5} className='mt-2 mt-md-0'>
          <FormControl className='form-control__district'>
            <Select
              displayEmpty
              className='rounded-pill border'
              value={districtInput}
              onChange={validateDistrict}
              onBlur={validateDistrict}
              onFocus={() => setDistrictIsTouched(true)}
            >
              <MenuItem value="0" readOnly={true} hidden={true} className='form-control__placeholder'>
                <em>Select District</em>
              </MenuItem>
              {districts.map(district => {
                return <MenuItem key={district.district_id} value={district.district_id} className='select-option'>{district.district_name}</MenuItem>
              })}
            </Select>
            {districtIsTouched && districtIsInValid && <FormHelperText>Please select correct district</FormHelperText>}
          </FormControl>
        </MDBCol>

        <MDBCol sm={12} md={2}>
          <Button variant='primary' type='submit' className='btn-rounded searchBtn w-100 w-md-75 mt-2 mt-md-0' >Search</Button>
        </MDBCol>
      </MDBRow>
    </Form>
  )
}

export default SearchByDistrict
