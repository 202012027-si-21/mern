import React from 'react';
import { Col } from 'react-bootstrap';

const HelplineItem = ({ itemTitle, numValue, numText = null }) => {
	return (
		<Col xs={6} sm={3} md={2} className='align-middle px-3'>
			<h6 className='p-0 m-0'>{itemTitle}</h6>
			<a href={`tel:${numValue}`}>{numText ? numText : numValue}</a>
		</Col>
	);
};

export default HelplineItem;
