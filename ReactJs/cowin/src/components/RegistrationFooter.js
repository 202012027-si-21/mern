import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'

const RegistrationFooter = () => {
  return (
    <MDBRow className='d-flex justify-content-center reg-footer py-3 align-self-end'>
      <MDBCol xs={12} md={8}>
        <div className='reg-footer__section d-flex justify-content-center text-white align-items-center'>
          <div className='d-flex align-items-center flex-wrap flex-column flex-md-row'>
            <ul className="m-0 p-0">
              <li><a className='border-end' href="https://www.cowin.gov.in/privacy-policy" target="_blank" rel="noreferrer">Privacy Policy</a></li>
              <li><a href="https://www.cowin.gov.in/terms-condition" target="_blank" rel="noreferrer">Terms of Service</a></li>
            </ul>
            <p> Copyright © 2021 COWIN. All Rights Reserved </p>
          </div>
        </div>
      </MDBCol>
    </MDBRow>
  )
}

export default RegistrationFooter
