import React from 'react';
import { Spinner } from 'react-bootstrap';

const Loader = () => {
  return (
    <div className='loader'>
      <Spinner
        animation='border'
        role='status'
        variant='primary'
        style={{
          width: '40px',
          height: '40px',
        }}>
        <span className='sr-only'>Loading..</span>
      </Spinner>
    </div>
  );
};

export default Loader;
