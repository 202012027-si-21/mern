import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Container, Navbar, Nav, NavDropdown, Image } from 'react-bootstrap';
import covidLogo from '../assets/images/covid19logo.jpg';

const MainHeader = () => {
  const location = useLocation();
  const showSignInBtn = location.pathname !== '/selfregistration' && location.pathname !== '/dashboard'

  return (
    <header>
      <Container fluid className='px-0'>
        <Navbar className='py-2 py-lg-0 px-2' bg='white' variant='light' expand='lg'>
          <Container className='px-1 px-md-0' fluid='md'>
            <Navbar.Brand href='#' className='m-0 m-sm-1' variant='light'>
              <span>
                <Image
                  alt=''
                  src={covidLogo}
                  className='align-top secondaryHeader-brand__img'
                  fluid
                />
              </span>
            </Navbar.Brand>

            {showSignInBtn && <span className='order-lg-2 ms-0 ms-sm-auto'>
              <Link
                to='/selfregistration'
                id='registrationNavLink'
                className='btn btn-rounded btn-yellow py-2 px-4'>
                register / sign in
              </Link>
            </span>}

            <Navbar.Toggle />

            <Navbar.Collapse className='mx-3'>
              <Nav
                className={`ms-auto my-2 my-lg-0 ${showSignInBtn ? 'me-auto' : ''} `}>
                <NavDropdown
                  title='Vacination Services'
                  id='navbarScrollingDropdown1'>
                  <Nav.Link href='https://selfregistration.cowin.gov.in/selfregistration' target='_blank'>Register Members</Nav.Link>
                  <Nav.Link href='#searchSection'>Search Vaccination Centers</Nav.Link>
                  <Nav.Link href='https://selfregistration.cowin.gov.in/' target='_blank'>Book Vaccination Slots</Nav.Link>
                  <Nav.Link href='https://selfregistration.cowin.gov.in/selfregistration' target='_blank'>Manage Appointment</Nav.Link>
                  <Nav.Link href='https://selfregistration.cowin.gov.in/selfregistration' target='_blank'>Download Certificate</Nav.Link>
                </NavDropdown>

                <NavDropdown title='Platforms' id='navbarScrollingDropdown2'>
                  <Nav.Link href='https://sandbox.cowin.gov.in/' target='_blank'>CoWIN International</Nav.Link>
                  <Nav.Link href='https://app.cowin.gov.in/login' target='_blank'>Vaccinator</Nav.Link>
                  <Nav.Link href='https://admin.cowin.gov.in/' target='_blank'>Department Login</Nav.Link>
                  <Nav.Link href='https://verify.cowin.gov.in/' target='_blank'>Verify Certificate</Nav.Link>
                  <Nav.Link href='https://dashboard.cowin.gov.in/' target='_blank'>Vaccination Statistics</Nav.Link>
                </NavDropdown>

                <NavDropdown title='Resources' id='navbarScrollingDropdown3'>
                  <Nav.Link href='https://prod-cdn.preprod.co-vin.in/assets/pdf/User_Guide_Citizen%20registration_18%2B.pdf' target='_blank'>How To Get Vaccinated</Nav.Link>
                  <Nav.Link href='https://prod-cdn.preprod.co-vin.in/assets/pdf/Dos_and_Donts_for_Citizens.pdf' target='_blank'>Dos and Don'ts</Nav.Link>
                  <Nav.Link href='https://prod-cdn.preprod.co-vin.in/assets/pdf/CoWIN_Overview.pdf' target='_blank'>Overview</Nav.Link>
                  <Nav.Link href='https://prod-cdn.preprod.co-vin.in/assets/docx/CoWINAPIGuidelinesFinal.pdf' target='_blank'>API Guidelines</Nav.Link>
                  <Nav.Link href='https://prod-cdn.preprod.co-vin.in/assets/pdf/Grievance_Guidelines.pdf' target='_blank'>Grievance Guidelines</Nav.Link>
                </NavDropdown>

                <NavDropdown title='Support' id='navbarScrollingDropdown4'>
                  <Nav.Link href='https://www.cowin.gov.in/faq' target='_blank'>Frequently Asked Questions</Nav.Link>
                  <Nav.Link href='https://selfregistration.cowin.gov.in/selfregistration' target='_blank'>Certificate Corrections</Nav.Link>
                  <Nav.Link href='#contactUsSection'>Contact us</Nav.Link>
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </Container>
    </header>
  );
};

export default MainHeader;
