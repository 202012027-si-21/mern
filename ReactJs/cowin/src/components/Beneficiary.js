import React from 'react'
import { useSelector } from 'react-redux';
import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import { Trash } from 'react-bootstrap-icons'
import Dose1 from './doses/Dose1';
import Dose2 from './doses/Dose2';

const Beneficiary = ({ beneficiary }) => {
  const metadata = useSelector((state) => state.metadata);
  const { idTypes } = metadata;

  const getPhotoId = (id) => {
    if (typeof id === 'string') {
      return id
    }

    for (const type in idTypes) {
      if (idTypes[type].id === id) {
        return idTypes[type].type
      }
    }
  }

  const doses_data = beneficiary.appointments
  const dose1_taken = doses_data.length > 0 && beneficiary.dose1_date.length > 0


  return (
    <MDBRow className='beneficiary__container'>
      <MDBCol xs={12}>
        <div className="cardBlocks py-2">
          <MDBRow className='d-flex flex-wrap'>
            <MDBCol className='position-relative' xs={12}>
              <span className={`vaccination_status ${beneficiary.vaccination_status === 'Not Vaccinated' ? 'not-vaccinated' : ''}`}>{beneficiary.vaccination_status}</span>
              <div className="nameBox mt-3">
                <span>
                  <h3 className='nameBox__content'>
                    <span>{beneficiary.name}</span>
                    <p>
                      REF ID : <span>{beneficiary.beneficiary_reference_id}</span>
                    </p>
                    <p>
                      Secret Code : <span>{`${beneficiary.beneficiary_reference_id.charAt(beneficiary.beneficiary_reference_id.length - 4)}${beneficiary.beneficiary_reference_id.charAt(beneficiary.beneficiary_reference_id.length - 3)}${beneficiary.beneficiary_reference_id.charAt(beneficiary.beneficiary_reference_id.length - 2)}${beneficiary.beneficiary_reference_id.charAt(beneficiary.beneficiary_reference_id.length - 1)}`}</span>
                    </p>
                  </h3>
                </span>
                {beneficiary.vaccination_status === 'Not Vaccinated' && <span style={{ cursor: 'pointer' }}>
                  <Trash width={25} height={25}></Trash>
                </span>}
              </div>
            </MDBCol>
          </MDBRow>

          <MDBRow className='mt-2'>
            <MDBCol xs={12} md={3}>
              <span className='labelcls'>Year of Birth: </span>
              <span className='valuecls'>{beneficiary.birth_year}</span>
            </MDBCol>

            <MDBCol xs={12} md={5}>
              <span className='labelcls'>Photo ID: </span>
              <span className='valuecls'>{getPhotoId(beneficiary.photo_id_type)}</span>
            </MDBCol>

            <MDBCol xs={12} md={3}>
              <span className='labelcls'>ID Number: </span>
              <span className='valuecls'>XXXX-{`${beneficiary.photo_id_number.charAt(beneficiary.photo_id_number.length - 4)}${beneficiary.photo_id_number.charAt(beneficiary.photo_id_number.length - 3)}${beneficiary.photo_id_number.charAt(beneficiary.photo_id_number.length - 2)}${beneficiary.photo_id_number.charAt(beneficiary.photo_id_number.length - 1)}`}</span>
            </MDBCol>
          </MDBRow>
          <div className='mt-2'>
            <Dose1 beneficiary={beneficiary}></Dose1>
            {dose1_taken && <Dose2 beneficiary={beneficiary}></Dose2>}
          </div>
        </div>
      </MDBCol>
    </MDBRow>
  )
}

export default Beneficiary
