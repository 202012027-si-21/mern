import React from 'react';
import { Carousel, Image, Row, Col, Container } from 'react-bootstrap';

import slider1Img from '../assets/images/slider-image-1.png';
import slider2Img from '../assets/images/slider-image-2.jpg';

const HomePageCarousel = () => {
  return (
    <section id='carouselSection'>
      <Carousel
        id='carouselExampleIndicators'
        fade
        nextLabel={null}
        prevLabel={null}
        indicators={true}
        interval={3000}>
        <Carousel.Item>
          <Container fluid='xl' className='py-0 pt-md-3'>
            <Row>
              <Col sm={12} md={8}>
                <div className='display-2'>
                  <h1 className='display-2__title text-capitalise text-secondary'>
                    Are You Protected <br /> Against COVID-19?
                  </h1>
                  <p className='display-2__subtitle text-secondary'>
                    क्या आप कोविड-19 से सुरक्षित हैं ?
                  </p>
                </div>
                <Row className='carousel__buttons mt-5'>
                  <Col xs={12} sm={6}>
                    <a className='btn btn-rounded btn-yellow w-100 shadow text-primary' href='https://selfregistration.cowin.gov.in/' target='_blank' rel='noreferrer'>
                      book your slot <span>अपना स्लॉट बुक कर</span>
                    </a>
                  </Col>
                  <Col xs={12} sm={6} className='my-2 my-sm-0'>
                    <a href='https://verify.cowin.gov.in/' target='_blank' rel='noreferrer' className='btn btn-rounded btn-light border-primary w-100 shadow text-primary'>
                      download certificate <span>प्रमाणपत्र डाउनलोड करे</span>
                    </a>
                  </Col>
                </Row>
              </Col>
              <Col
                md={4}
                sm={12}
                className='text-center text-md-end carousel__image'>
                <Image src={slider1Img}></Image>
              </Col>
            </Row>
          </Container>
        </Carousel.Item>

        <Carousel.Item className='carousel-item-banner-bg'>
          <Container fluid='xl' className='py-0 py-md-5'>
            <Row className='gx-5'>
              <Col className='px-0 px-md-5' sm={12} md={6}>
                <div className='display-3'>
                  <h1 className='display-3__title text-capitalise text-secondary'>
                    CoWIN Is Helping Countries Worldwide To Run Vaccination
                    Drives
                  </h1>
                  <p className='display-3__subtitle text-secondary'>
                    Launching CoWIN as an open source platform for countries to
                    orchestrate successful vaccination with efficient monitoring
                    with an aim of achieving universal vaccination
                  </p>
                </div>
                <div className='mb-4 text-center text-md-start'>
                  <a
                    href='https://sandbox.cowin.gov.in/'
                    target='_blank'
                    rel='noreferrer'
                    className='btn btn-primary btn-rounded py-1 px-4 text-capitalise fw-normal'
                    style={{ fontSize: '12px' }}>
                    know more
                  </a>
                </div>
              </Col>
              <Col sm={0} md={1}></Col>

              <Col className='text-center carousel__image' sm={12} md={5}>
                <Image src={slider2Img} fluid></Image>
              </Col>
            </Row>
          </Container>
        </Carousel.Item>
      </Carousel>
    </section>
  );
};

export default HomePageCarousel;
