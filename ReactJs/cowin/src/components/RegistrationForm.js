import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Form } from 'react-bootstrap'
import logo from '../assets/images/login-screen.svg'
import aarogya from '../assets/images/arogya.png'
import umang from '../assets/images/UMANG-Logo.png'
import { confirmOTP, sendOTP } from '../actions/userActions'
import Loader from '../components/Loader'
import { BENEFICIARIES_DATA_RESET } from '../constants/userConstants'

const TIMER_SEC = 180

const RegistrationForm = ({ history }) => {
  const [mobileInput, setMobileInput] = useState('');
  const [mobileIsInValid, setMobileIsInValid] = useState('');
  const [mobileIsTouched, setMobileIsTouched] = useState(false);

  const [otpSent, setOtpSent] = useState(false)

  const [otpInput, setOtpInput] = useState('');

  const [seconds, setSeconds] = useState(TIMER_SEC)

  const dispatch = useDispatch()
  const userSendOTP = useSelector((state) => state.userSendOTP);
  const { loading, number } = userSendOTP;

  const userConfirmOTP = useSelector((state) => state.userConfirmOTP);
  const { loading: loadingOTP, error: errorOTP, success: successOTP, token } = userConfirmOTP;

  useEffect(() => {
    let optInterval;

    if (successOTP && token.length > 0) {
      // dispatch({ type: BENEFICIARIES_DATA_RESET })
      history.push('/dashboard')
    }

    if (otpSent) {
      optInterval = setInterval(() => {
        if (seconds > 0) {
          setSeconds(seconds - 1)
        } else {
          clearInterval(optInterval)
        }
      }, 1000);
    }

    return () => {
      clearInterval(optInterval)
    };
  }, [seconds, otpSent, history, successOTP, token, dispatch]);

  const validateMobile = (e) => {
    if (e.target.value.trim().length === 0) {
      setMobileIsInValid('')
      return false
    }
    if (e.target.value.trim().length === 10) {
      if (e.target.value.trim()[0] === '6' || e.target.value.trim()[0] === '7' || e.target.value.trim()[0] === '8' || e.target.value.trim()[0] === '9') {
        setMobileIsInValid('')
      } else {
        setMobileIsInValid('Mobile Number Must Starts with 6, 7, 8 or 9')
      }
    } else {
      setMobileIsInValid('Must be 10 digits')
    }
    setMobileInput(e.target.value.replace(/[^0-9]/g, ''))
  }

  const validateOTP = (e) => {
    setOtpInput(e.target.value.replace(/[^0-9]/g, ''))
  }

  const sendOTPHandler = () => {
    dispatch(sendOTP(mobileInput))
  }

  const mobileSubmitHandler = (e) => {
    e.preventDefault()
    if (!otpSent) {
      sendOTPHandler()
      setOtpSent(true)
    } else {
      dispatch(confirmOTP(otpInput))
    }
  }

  const resendHandler = () => {
    if (otpSent && number.length === 10) {
      setSeconds(TIMER_SEC)
      dispatch(sendOTP(number))
    }
  }

  return (
    <MDBCol xs={12} className='registration-form__box'>
      {loading && <Loader></Loader>}
      {loadingOTP && <Loader></Loader>}
      <Form className='registration-form py-3' onSubmit={mobileSubmitHandler}>
        <MDBRow className='d-flex flex-wrap'>
          {!otpSent && (<MDBCol xs={12} className='registration-form__header'>
            <div className="image-icon d-flex justify-content-center align-items-center">
              <img src={logo} alt="Logo" />
            </div>
            <h2>Register or Sign In For Vaccination</h2>
            <p>An OTP will be sent to your mobile number for verification</p>
          </MDBCol>)}

          {otpSent && (<MDBCol xs={12} className='registration-form__header'>
            <div className="image-icon d-flex justify-content-center align-items-center">
              <img src={logo} alt="Logo" />
            </div>
            <h2>OTP Verification</h2>
            <p className='mb-0'>An OTP has been sent to</p>
            <p className='mt-0'>XXX XXX {`${mobileInput[6]}${mobileInput[7]}${mobileInput[8]}${mobileInput[9]}`}</p>
          </MDBCol>)}

          <MDBCol xs={12}>
            {!otpSent && <Form.Group md="4" controlId="validationCustom01">
              <Form.Control
                required
                type="text"
                placeholder="Enter your mobile number"
                value={mobileInput}
                onChange={validateMobile}
                onBlur={validateMobile}
                onFocus={() => setMobileIsTouched(true)}
                isInvalid={mobileIsTouched && mobileIsInValid !== ''}
                maxLength={10}
              />
              <Form.Control.Feedback type="invalid" className='input__error' style={{ fontSize: '14px', fontWeight: 400 }}>
                {mobileIsInValid}
              </Form.Control.Feedback>
            </Form.Group>}

            {otpSent && <Form.Group md="4" controlId="validationCustom02">
              <Form.Control
                required
                type="text"
                placeholder="Enter OTP"
                value={otpInput}
                onChange={validateOTP}
                isInvalid={errorOTP !== ''}
                maxLength={6}
              />
              <Form.Control.Feedback type="invalid" className='input__error' style={{ fontSize: '14px', fontWeight: 400 }}>
                {errorOTP}
              </Form.Control.Feedback>
            </Form.Group>}

            {otpSent && <div className="text-center timer__container mt-3">
              {seconds > 0 && <p className='timer'>{seconds} sec</p>}
              {seconds === 0 && <span className='resend-otp-btn' onClick={() => { resendHandler() }}>Resend OTP</span>}
              <p className='delay-text'>There might be some delay in receiving the OTP due to heavy traffic</p>
            </div>
            }


            <Button variant="primary w-100" size="lg" type='submit'>
              {otpSent ? 'Verify & Proceed' : 'GET OTP'}
            </Button>
          </MDBCol>
          {!otpSent && <p className='registration-form__links'>By Sign In/Registration, I agree to the
            <a target='_blank' rel='noreferrer' href="https://www.cowin.gov.in/terms-condition">Terms of Service</a> and <a target='_blank' rel='noreferrer' href="https://www.cowin.gov.in/privacy-policy">Privacy Policy</a>
          </p>}
        </MDBRow>
      </Form>
      {!otpSent && <div className="other-logins">
        <p>or book appointment using</p>
      </div>}

      {!otpSent && <div className="using-other-block d-flex justify-content-between">
        <div className="arogya-login">
          <a href="https://www.aarogyasetu.gov.in/" target="_blank" rel="noreferrer" role="button">
            <span>
              <img src={aarogya} alt="Aarogya logo" />
              <span>Aarogya Setu</span></span>
          </a>
        </div>

        <div className="umang-login">
          <a href="https://web.umang.gov.in/web_new/login?redirect_to=department%3Furl%3Dcowin%2F%26dept_id%3D355%26dept_name%3DCo-WIN" target="_blank" rel="noreferrer" role="button">
            <span>
              <img src={umang} alt="Umang logo" />
            </span>
          </a>
        </div>
      </div>}
    </MDBCol>
  )
}

export default RegistrationForm
