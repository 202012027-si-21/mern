import React from 'react'
import moment from 'moment'

const DataRow = ({ startDate, columns, sessions }) => {
  const newSessions = []
  const endDate = moment(startDate, "DD-MM-YYYY").add(columns, 'd')

  for (const m = moment(startDate, "DD-MM-YYYY"); m.isBefore(endDate); m.add(1, 'days')) {
    const sessionsPerDay = sessions.filter((session) => {
      return session.date === m.format("DD-MM-YYYY")
    })

    newSessions.push(<div className='d-flex flex-column align-items-center slots' key={m.unix()}>
      {sessionsPerDay.length > 0 ? sessionsPerDay.map(s => {
        const ageText = s.allow_all_age ? '18 & Above' : s.min_age_limit === 45 ? '45 & Above' : '18-44 Only'
        return (s.available_capacity_dose1 > 0 || s.available_capacity_dose2 > 0) ? (
          <div className='slot-wrap slot-info' key={s.session_id}>
            <h5>{s.vaccine}</h5>
            <div className='dosetotal'>
              <span>
                <p>D1</p>{s.available_capacity_dose1}
              </span>
              <a className={`totalSlts ${s.available_capacity_dose1 + s.available_capacity_dose2 <= 10 && 'lessSlots'}`} href="/">{s.available_capacity_dose1 + s.available_capacity_dose2}</a>
              <span>
                <p>D2</p>{s.available_capacity_dose2}
              </span>
            </div>
            <div className='age-limit'>
              <span>{ageText}</span>
            </div>
          </div>) : (
          <div className='noslot-wrap slot-info' key={s.session_id}>
            <h5>{s.vaccine}</h5>
            <a href="/">Booked</a>
            <div className='age-limit'><span>{ageText}</span></div>
          </div>)
      }) : <p className='notAvailableLabel'>NA</p>}
    </div>)
  }


  return (
    <div className='d-flex flex-row justify-content-evenly'>
      {columns > 1 ? newSessions : "Mobile"}
    </div>
  )
}

export default DataRow
