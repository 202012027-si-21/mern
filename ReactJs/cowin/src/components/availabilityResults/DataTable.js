import React from 'react'
import DataTableDesktop from './DataTableDesktop'

const DataTable = ({ columns }) => {
  return (
    <div id='data-table'>
      <DataTableDesktop columns={columns}></DataTableDesktop>
    </div>
  )
}

export default DataTable
