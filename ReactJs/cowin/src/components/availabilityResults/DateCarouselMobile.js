import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { MDBContainer } from 'mdb-react-ui-kit'
import React from 'react'
import moment from 'moment';
import { getAvailabilityList } from '../../actions/availabilityActions'

const DateCarouselMobile = ({ columns }) => {
  const dispatch = useDispatch()

  const availabilityList = useSelector(state => state.availabilityList);
  const { startDate, currentDate } = availabilityList
  const endDate = moment(startDate, "DD-MM-YYYY")
  endDate.add(11, 'w')

  const dateChangeHandler = useCallback((e) => {
    e.preventDefault();
    const newDate = e.currentTarget.getAttribute("date-value")
    dispatch(getAvailabilityList(newDate))
  }, [dispatch])

  const dates = []
  for (var m = moment(startDate, "DD-MM-YYYY"); m.isBefore(endDate); m.add(1, 'days')) {
    var d = m.format("DD-MM-YYYY")
    dates.push(<li key={m.unix()} className={m.format("DD-MM-YYYY") === currentDate ? 'date-active' : ''}>
      <span onClick={dateChangeHandler} date-value={d}>
        <p> {m.format("D MMM")}</p>
      </span>
    </li>)
  }

  return (
    <MDBContainer breakpoint='xl'>
      <ul className='date-list'>
        {dates}
      </ul>
    </MDBContainer>
  )
}

export default DateCarouselMobile
