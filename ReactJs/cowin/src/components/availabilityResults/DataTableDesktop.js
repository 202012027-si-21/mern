import React from 'react'
import { useSelector } from 'react-redux'
import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit'
import DataRow from './DataRow';

const DataTableDesktop = ({ columns }) => {
  const availabilityList = useSelector(state => state.availabilityList);
  const { filteredCenters, startDate, currentDate } = availabilityList

  const rows = []
  if (columns === 1) {
    filteredCenters.forEach(center => {
      center.sessions.forEach(session => {
        if (currentDate === session.date) {
          const ageText = session.allow_all_age ? '18 & Above' : session.min_age_limit === 45 ? '45 & Above' : '18-44 Only'
          rows.push(
            <div className='center-info-mobile' key={session.session_id}>
              <div className='d-flex appointment-info'>
                <div className='centers-details centers-details__mobile'>
                  <h5>{center.name}</h5>
                  <p>{center.address}, {center.district_name}, {center.state_name}, {center.pincode}</p>
                </div>
                <div className='vaccine-info'>
                  <h5>{session.vaccine}</h5>
                  <div>
                    {center.fee_type === 'Free' && <span className='tag-free'>{center.fee_type}</span>}
                    {center.fee_type === 'Paid' && <span className='tag-paid'>{center.fee_type}</span>}
                  </div>
                </div>
              </div>
              <div className='dose-details__mobile'>
                <div className='d-flex'>
                  <div className='filter-block'><p>{ageText}</p></div>
                  <div className='dose1-block'>
                    <span className='dose-title'>Dose 1</span>
                    {session.available_capacity_dose1 > 0 ? <span className={session.available_capacity_dose1 < 10 ? 'lessSlots__mobile' : ''}>{session.available_capacity_dose1} slots</span> : <span>✖</span>}
                  </div>
                  <div className='dose2-block'>
                    <span className='dose-title'>Dose 2</span>
                    {session.available_capacity_dose2 > 0 ? <span className={session.available_capacity_dose2 < 10 ? 'lessSlots__mobile' : ''}>{session.available_capacity_dose2} slots</span> : <span>✖</span>}
                  </div>
                </div>
              </div>
            </div >)
        }
      })
    });
  }

  return (
    <MDBContainer breakpoint='xl'>
      <MDBRow>
        <MDBCol className='date-table-container'>
          {filteredCenters.length > 0 && columns > 1 && filteredCenters.map((center) => (
            <MDBRow key={center.center_id} className='py-3 center-row border-bottom'>
              <MDBCol md={3}>
                <div className='centers-details px-1 px-lg-3'>
                  <h5>{center.name} {center.fee_type === 'Paid' && <span>{center.fee_type}</span>}</h5>
                  <p>{center.address}, {center.district_name}, {center.state_name}, {center.pincode}</p>
                  {center.vaccine_fees && (
                    <ul>
                      {center.vaccine_fees.map((vaccine) => (<li key={vaccine.vaccine}>{vaccine.vaccine}: {vaccine.fee}</li>))}
                    </ul>
                  )}
                </div>
              </MDBCol>
              <MDBCol md={9}>
                <DataRow startDate={startDate} columns={columns} sessions={center.sessions}></DataRow>
              </MDBCol>
            </MDBRow>
          ))}
          {filteredCenters.length > 0 && columns === 1 && rows.length > 0 && rows}
          {filteredCenters.length === 0 && <p className='text-center mt-5 fw-bold'>No Vaccination center is available for booking.</p>}
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  )
}

export default DataTableDesktop
