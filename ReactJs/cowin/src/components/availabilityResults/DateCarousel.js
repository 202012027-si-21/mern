import React from 'react'
// import { MDBRow, MDBCol } from 'mdb-react-ui-kit'
import DateCarouselMobile from './DateCarouselMobile'
import DateCarouselDesktop from './DateCarouselDesktop'

const DateCarousel = ({ columns }) => {
  return (
    <div id='date-carousel'>
      {columns === 0 ? <DateCarouselMobile></DateCarouselMobile> : <DateCarouselDesktop columns={columns}></DateCarouselDesktop>}
    </div>
  )
}

export default DateCarousel
