import React from 'react'
// import { useSelector } from 'react-redux'
import MediaQuery from 'react-responsive'
import DateCarousel from './DateCarousel'
import DateCarouselMobile from './DateCarouselMobile'
import DataTable from './DataTable'

const AvailabilityTable = () => {
  console.log("AvailabilityTable")
  return (<>
    <MediaQuery minWidth={992}>
      <DateCarousel columns={7}></DateCarousel>
      <DataTable columns={7}></DataTable>
    </MediaQuery>

    <MediaQuery maxWidth={992} minWidth={775}>
      <DateCarousel columns={5}></DateCarousel>
      <DataTable columns={5}></DataTable>
    </MediaQuery>

    <MediaQuery maxWidth={775}>
      <DateCarouselMobile columns={5}></DateCarouselMobile>
      <DataTable columns={1}></DataTable>
    </MediaQuery>
  </>
  )
}

export default AvailabilityTable
