import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { MDBCheckbox } from 'mdb-react-ui-kit'
import { updateFilters } from '../../actions/filterActions'

const FilterResults = () => {
  console.log("Filter")
  const availabilityList = useSelector(state => state.availabilityList);
  const { filters } = availabilityList;

  const { age: ageFilters, cost: costFilters, vaccine: vaccineFilters } = filters

  const ageFiltersArr = Object.entries(ageFilters).filter((ele) => ele[1].selected)
  const ageFiltersStr = ageFiltersArr.map((ele) => ele[1].label).join(', ')

  const costFiltersArr = Object.entries(costFilters).filter((ele) => ele[1].selected)
  const costFiltersStr = costFiltersArr.map((ele) => ele[1].label).join(', ')

  const vaccineFiltersArr = Object.entries(vaccineFilters).filter((ele) => ele[1].selected)
  const vaccineFiltersStr = vaccineFiltersArr.map((ele) => ele[1].label).join(', ')

  const dispatch = useDispatch()
  const filterChangeHandler = (filterType, filter) => {
    dispatch(updateFilters(filterType, filter))
  }

  return (<div className='mb-5 px-3'>
    {
      (ageFiltersArr.length > 0 || costFiltersArr.length || vaccineFiltersArr.length > 0) && (
        <div className='d-flex flex-column flex-md-row color-gray mb-2'>
          <div>
            <p className='d-block me-1 filter-results__text mb-0' style={{ fontSize: '0.8rem', fontWeight: 500 }}>Showing results for:</p>
          </div>
          <div className='d-flex flex-row'>
            {ageFiltersArr.length > 0 && (
              <div className='border-start px-md-3 filterResults__row'>
                <p className='d-block mb-0 me-1 filter-results__element color-gray'>Age: <span>{ageFiltersStr}</span></p>
              </div>
            )}
            {costFiltersArr.length > 0 && (
              <div className='border-start px-md-3 filterResults__row'>
                <p className='d-block mb-0 me-1 filter-results__element color-gray'>Cost: <span>{costFiltersStr}</span></p>
              </div>
            )}
            {vaccineFiltersArr.length > 0 && (
              <div className='border-start px-md-3 filterResults__row'>
                <p className='d-block mb-0 me-1 filter-results__element color-gray'>Vaccine: <span>{vaccineFiltersStr}</span></p>
              </div>
            )}
          </div>

        </div>
      )
    }

    <div className='d-flex flex-column flex-md-row '>
      <div className='d-flex align-items-end mb-0'>
        <p className='d-block mb-0 me-1 filter-results__text' style={{ fontSize: '0.7rem' }}>Filter results by:</p>
      </div>
      <div className='px-md-3 border-start filterResults__row'>
        <div className='filterResults__title'>Age</div>
        <div sm={12} className='filterResults__btn-group d-flex flex-wrap'>
          <MDBCheckbox name='btnCheck' btn id='btn-18-plus' btnColor='#fff' wrapperTag='span' label='18 & Above' onChange={(() => filterChangeHandler('age', '18-plus'))} checked={ageFilters['18-plus']['selected']} />
          <MDBCheckbox name='btnCheck' btn id='btn-18-44' btnColor='#fff' wrapperTag='span' label='18-44 Only' onChange={(() => filterChangeHandler('age', '18-44'))} checked={ageFilters['18-44']['selected']} />
          <MDBCheckbox name='btnCheck' btn id='btn-45-plus' btnColor='#fff' wrapperTag='span' label='45 & Above' onChange={(() => filterChangeHandler('age', '45-plus'))} checked={ageFilters['45-plus']['selected']} />
        </div>
      </div>

      <div className='px-md-3 border-start filterResults__row'>
        <div className='filterResults__title'>Cost</div>
        <div sm={12} className='filterResults__btn-group d-flex flex-wrap'>
          <MDBCheckbox name='btnCheck' btn id='btn-paid' btnColor='#fff' wrapperTag='span' label='Paid' onChange={(() => filterChangeHandler('cost', 'paid'))} checked={costFilters['paid']['selected']} />
          <MDBCheckbox name='btnCheck' btn id='btn-free' btnColor='#fff' wrapperTag='span' label='Free' onChange={(() => filterChangeHandler('cost', 'free'))} checked={costFilters['free']['selected']} />
        </div>
      </div>

      <div className='px-md-3 border-start filterResults__row'>
        <div className='filterResults__title'>Vaccine</div>
        <div sm={12} className='filterResults__btn-group d-flex flex-wrap'>
          <MDBCheckbox name='btnCheck' btn id='btn-covishield' btnColor='#fff' wrapperTag='span' label='Covishield' onChange={(() => filterChangeHandler('vaccine', 'covishield'))} checked={vaccineFilters['covishield']['selected']} />
          <MDBCheckbox name='btnCheck' btn id='btn-covaxin' btnColor='#fff' wrapperTag='span' label='COVAXIN' onChange={(() => filterChangeHandler('vaccine', 'covaxin'))} checked={vaccineFilters['covaxin']['selected']} />
          <MDBCheckbox name='btnCheck' btn id='btn-sputnik' btnColor='#fff' wrapperTag='span' label='SPUTNIK V' onChange={(() => filterChangeHandler('vaccine', 'sputnik'))} checked={vaccineFilters['sputnik']['selected']} />
        </div>
      </div>

    </div >
  </div>)
}

export default FilterResults