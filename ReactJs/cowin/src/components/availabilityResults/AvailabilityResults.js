import React, { useState } from 'react'
import { MDBContainer, MDBIcon } from 'mdb-react-ui-kit'
import { useSelector } from 'react-redux';
import { InfoCircle, Save2, Trash } from 'react-bootstrap-icons'
import FilterResults from './FilterResults';
import AvailabilityTable from './AvailabilityTable';
import { Button, Spinner } from 'react-bootstrap';

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const AvailabilityResultsIntro = () => {
  const [saveLoader, setSaveLoader] = useState(false)
  const [deleteLoader, setDeleteLoader] = useState(false)

  const availabilityList = useSelector(state => state.availabilityList);
  const { filteredCenters, searchType, searchStatus, searchParams } = availabilityList;

  const savePreferences = async () => {
    setSaveLoader(true)
    if (searchStatus) {
      localStorage.setItem('searchType', searchType)
      localStorage.setItem('searchParams', JSON.stringify(searchParams))
    }
    await sleep(800)
    setSaveLoader(false)

  }

  const deletePreferences = async () => {
    setDeleteLoader(true)
    if (searchStatus) {
      localStorage.removeItem('searchType')
      localStorage.removeItem('searchParams')
    }
    await sleep(800)
    setDeleteLoader(false)
  }

  return <>
    <div className='d-block d-md-none text-primary border mx-1 py-3 px-3 px-md-5 mb-3'>
      <h5 style={{ fontSize: '17px', fontWeight: 700 }}><InfoCircle width={16} height={16}></InfoCircle> Information regarding slots & centres</h5>
      <ul className='fs-7 text-gray-info lh-base mb-0 ps-3'>
        <li className='pb-2'><span className='text-secondary' style={{ fontWeight: 700 }}><MDBIcon icon="walking" /> Walk-in available</span> at all vaccination centers for age 18 years and above (For timings for walk-in vaccinations, please contact the vaccine center.)</li>
        <li className='pb-2'>Slots are updated by state vaccination centers and private hospitals everyday at 8AM, 12PM, 4PM, & 8PM.</li>
      </ul>
    </div>

    <div className='d-flex align-items-center mb-3 flex-lg-row flex-column'>
      <h4 className='availability-count px-3'>Slot Search Results <span>({filteredCenters.length} center(s) found)</span></h4>
      <div className='d-flex'>
        <Button className='mx-1 mx-md-5 preference-button btn-secondary' onClick={savePreferences}>{saveLoader ? <Spinner
          as="span"
          animation="border"
          size="sm"
          role="status"
          aria-hidden="true"
          style={{ display: 'inline-block', marginRight: '10px' }}
        /> : <Save2 className='me-2' width={16} height={16}></Save2>}Save Search Settings</Button>
        <Button className='mx-0 mx-md-5 preference-button btn-danger' onClick={deletePreferences}>{deleteLoader ? <Spinner
          as="span"
          animation="border"
          size="sm"
          role="status"
          aria-hidden="true"
          style={{ display: 'inline-block', marginRight: '10px' }}
        /> : <Trash className='me-2' width={16} height={16}></Trash>}Reset Search Settings</Button>
      </div>
    </div>
  </>
}

const AvailabilityResults = () => {
  // const availabilityFilters = useSelector(state => state.availabilityFilters);
  // const { filters, filteredCenters } = availabilityFilters;

  const availabilityList = useSelector(state => state.availabilityList);
  const { searchStatus } = availabilityList

  return <>
    {searchStatus && (
      <MDBContainer className='my-4 my-md-5 p-0' breakpoint='xl'>
        <AvailabilityResultsIntro></AvailabilityResultsIntro>

        <FilterResults></FilterResults>

        <div className='d-none d-md-flex flex-row text-primary px-5'>
          <InfoCircle width={24} height={24}></InfoCircle>
          <ul className='fs-7 text-gray-info lh-lg'>
            <li>Slots are updated by state vaccination centers and private hospitals everyday at 8AM, 12PM, 4PM, & 8PM.</li>
            <li><span className='text-secondary' style={{ fontWeight: 700 }}><MDBIcon icon="walking" /> Walk-in available</span> at all vaccination centers for age 18 years and above (For timings for walk-in vaccinations, please contact the vaccine center.)</li>
            <li><span className='me-5'>D1 - Vaccine Dose #1</span><span>D2 - Vaccine Dose #2</span></li>
          </ul>
        </div>

        <AvailabilityTable />
      </MDBContainer>
    )} </>
}

export default AvailabilityResults
