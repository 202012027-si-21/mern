import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'
import dosePending from '../../assets/images/dosepending.svg'
import doseComplete from '../../assets/images/dosecomplete.svg'

const Dose2 = ({ beneficiary }) => {
  const doses_data = beneficiary.appointments
  const dose2_taken = doses_data.length > 0 && beneficiary.dose2_date.length > 0
  let dose2_details = null

  dose2_details = doses_data.find(ele => ele.dose === 2)
  const dose2_scheduled = dose2_details ? true : false

  let appointment_text = 'Appointment not scheduled'
  if (dose2_taken || dose2_scheduled) {
    appointment_text = `${dose2_details.name}, ${dose2_details.date}, ${dose2_details.slot}`
  }

  const content = (
    <MDBRow className='py-1 dose-data mx-2' >
      <MDBCol xs={12} md={7} lg={6} className={`py-1 px-0 ${dose2_taken ? 'dose-green' : 'dose-red'}`}>
        <span className="dose1-label">
          <span className="dose-complete">
            <img src={dose2_taken ? doseComplete : dose2_scheduled ? dosePending : dosePending} alt="" />
          </span>
          <span>
            Dose 2&nbsp;
          </span>
        </span>
        <div className="appointment-details">
          <span>{appointment_text}</span>
        </div>
      </MDBCol>
    </MDBRow >)

  return content
}

export default React.memo(Dose2)
