import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'
import dosePending from '../../assets/images/dosepending.svg'
import doseComplete from '../../assets/images/dosecomplete.svg'

const Dose1 = ({ beneficiary }) => {
  const doses_data = beneficiary.appointments
  const dose1_taken = doses_data.length > 0 && beneficiary.dose1_date.length > 0
  let dose1_details = null

  dose1_details = doses_data.find(ele => ele.dose === 1)
  const dose1_scheduled = dose1_details ? true : false

  let appointment_text = 'Appointment not scheduled'
  if (dose1_taken || dose1_scheduled) {
    appointment_text = `${dose1_details.name}, ${dose1_details.date}, ${dose1_details.slot}`
  }

  const content = (
    <MDBRow className='py-1 dose-data mx-2' >
      <MDBCol xs={12} md={7} lg={6} className={`py-1 px-0 ${dose1_taken ? 'dose-green' : 'dose-red'}`}>
        <span className="dose1-label">
          <span className="dose-complete">
            <img src={dose1_taken ? doseComplete : dose1_scheduled ? dosePending : dosePending} alt="" />
          </span>
          <span>
            Dose 1&nbsp;
          </span>
          {dose1_taken && <span className='vaccine-name'>
            {beneficiary.vaccine}
          </span>}
        </span>
        <div className="appointment-details">
          <span>{appointment_text}</span>
        </div>
      </MDBCol>
    </MDBRow >)

  return content
}

export default React.memo(Dose1)
