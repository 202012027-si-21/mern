import { MDBCol, MDBContainer, MDBRow } from 'mdb-react-ui-kit'
import React from 'react'

const Footer = () => {
  return (
    <MDBContainer id='footer' fluid>
      <MDBContainer breakpoint='xl'>
        <MDBRow>
          <MDBCol sm={12} lg={8}>
            <MDBRow>
              <MDBCol xs={12} sm={4} md={3}>
                <h4>Vaccination Services</h4>
                <ul>
                  <li><a href="https://selfregistration.cowin.gov.in/selfregistration" target="_blank" rel='noreferrer'>Register Members</a></li>
                  <li><a href="#searchSection">Search Vaccination Centers</a></li>
                  <li><a href="https://selfregistration.cowin.gov.in/selfregistration" target="_blank" rel='noreferrer'>Book Vaccination Slots</a></li>
                  <li><a href="https://selfregistration.cowin.gov.in/selfregistration" target="_blank" rel='noreferrer'>Manage Appointment</a></li>
                  <li><a href="https://selfregistration.cowin.gov.in/vaccination-certificate" target="_blank" rel='noreferrer'>Download Certificate</a></li>
                </ul>
              </MDBCol>

              <MDBCol xs={12} sm={4} md={3}>
                <h4>Platforms</h4>
                <ul>
                  <li><a href="https://sandbox.cowin.gov.in/" target="_blank" rel='noreferrer'>Cowin International</a></li>
                  <li><a href="https://app.cowin.gov.in/" target="_blank" rel='noreferrer'>Vaccinator</a></li>
                  <li><a href="https://admin.cowin.gov.in/" target="_blank" rel='noreferrer'>Department Login</a></li>
                  <li><a href="https://verify.cowin.gov.in/" target="_blank" rel='noreferrer'>Verify Certificates</a></li>
                  <li><a href="https://dashboard.cowin.gov.in/" target="_blank" rel='noreferrer'>Vaccination Statistics</a></li>
                </ul>
              </MDBCol>

              <MDBCol xs={12} sm={4} md={3}>
                <h4>Resources</h4>
                <ul>
                  <li><a href="https://prod-cdn.preprod.co-vin.in/assets/pdf/User_Guide_Citizen%20registration_18%2B.pdf" target="_blank" rel='noreferrer' download>How To Get Vaccinated</a></li>
                  <li><a href="https://prod-cdn.preprod.co-vin.in/assets/pdf/Dos_and_Donts_for_Citizens.pdf" target="_blank" rel='noreferrer' download>Dos and Don'ts</a></li>
                  <li><a href="https://prod-cdn.preprod.co-vin.in/assets/pdf/CoWIN_Overview.pdf" target="_blank" rel='noreferrer' download>Overview</a></li>
                  <li><a href="https://prod-cdn.preprod.co-vin.in/assets/docx/CoWINAPIGuidelinesFinal.pdf" download target="_blank" rel='noreferrer'>API Guidelines</a></li>
                  <li><a href="https://prod-cdn.preprod.co-vin.in/assets/pdf/Grievance_Guidelines.pdf" download target="_blank" rel='noreferrer'>Grievance Guidelines</a></li>
                </ul>
              </MDBCol>

              <MDBCol xs={12} sm={4} md={3}>
                <h4>Support</h4>
                <ul className='border-end supportUl'>
                  <li><a href="https://www.cowin.gov.in/faq" target="_blank" rel='noreferrer'>Frequesntly Asked Questions</a></li>
                  <li><a href="https://selfregistration.cowin.gov.in/selfregistration" target="_blank" rel='noreferrer'>Certificate Corrections</a></li>
                  <li><a href="#contactUsSection">Contact Us</a></li>
                </ul>
              </MDBCol>
            </MDBRow>
          </MDBCol>
          <MDBCol sm={12} lg={4}>
            <MDBRow>
              <MDBCol sm={12} className='footer__helpline'>
                <p>Helpline: <span>+ <a href="tel:911123978046">91 11 23978046</a> (Toll Free - <a href="tel:1075">1075</a>)</span></p>

                <p>Technical Helpline: <span><a href="tel:01204473222">0120 4473222</a></span></p>
              </MDBCol>
            </MDBRow>
          </MDBCol>
        </MDBRow>

        <MDBRow className='bottom-footer'>
          <MDBCol xs={12} lg={6} className='footer_copyright'>
            <p>Copyright © 2021 CoWIN. All Rights Reserved </p>
          </MDBCol>

          <MDBCol xs={12} lg={6} className='footer_links'>
            <MDBRow>
              <MDBCol xs={12} lg={4} className='ms-auto'>
                <a href="https://www.cowin.gov.in/terms-condition" target='_blank' rel='noreferrer'>Terms of Service</a>
              </MDBCol>
              <MDBCol xs={12} lg={4}>
                <a href="https://www.cowin.gov.in/privacy-policy" target='_blank' rel='noreferrer'>Privacy Policy</a>
              </MDBCol>
            </MDBRow>
          </MDBCol>
        </MDBRow>
      </MDBContainer>

    </MDBContainer>
  )
}

export default Footer
