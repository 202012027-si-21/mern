export const SEND_OTP_SUCCESS = 'SEND_OTP_SUCCESS';
export const SEND_OTP_FAIL = 'SEND_OTP_FAIL';
export const SEND_OTP_REQUEST = 'SEND_OTP_REQUEST'
export const SEND_OTP_RESET = 'SEND_OTP_RESET'

export const CONFIRM_OTP_SUCCESS = 'CONFIRM_OTP_SUCCESS';
export const CONFIRM_OTP_FAIL = 'CONFIRM_OTP_FAIL';
export const CONFIRM_OTP_REQUEST = 'CONFIRM_OTP_REQUEST'
export const CONFIRM_OTP_RESET = 'CONFIRM_OTP_RESET'

export const BENEFICIARIES_DATA_SUCCESS = 'BENEFICIARIES_DATA_SUCCESS';
export const BENEFICIARIES_DATA_FAIL = 'BENEFICIARIES_DATA_FAIL';
export const BENEFICIARIES_DATA_REQUEST = 'BENEFICIARIES_DATA_REQUEST'
export const BENEFICIARIES_DATA_RESET = 'BENEFICIARIES_DATA_RESET'