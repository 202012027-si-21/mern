export const COWIN_BASE_URL = 'https://cdn-api.co-vin.in/api';
export const COWIN_SANDBOX_URL = 'https://cdndemo-api.co-vin.in/api';
export const COWIN_SANDBOX_HEADERS = {
  headers: {
    "X-api-key": "3sjOr2rmM52GzhpMHjDEE1kpQeRxwFDr4YcBEimi",
    'Accept': 'application/json, text/plain, /',
    'Accept-Language': 'en-US,en;q=0.5',
    'Content-Type': 'application/json',
  }
};