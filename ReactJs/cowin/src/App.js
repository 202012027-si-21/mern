import { BrowserRouter as Router, Route } from 'react-router-dom';
import DashboardScreen from './screens/DashboardScreen';
import HomeScreen from './screens/HomeScreen';
import RegistrationScreen from './screens/RegistrationScreen';

function App() {
  return (
    <Router>
      <Route path='/' component={HomeScreen} exact></Route>
      <Route path='/selfregistration' component={RegistrationScreen}></Route>
      <Route path='/dashboard' component={DashboardScreen}></Route>
    </Router>
  );
}

export default App;
