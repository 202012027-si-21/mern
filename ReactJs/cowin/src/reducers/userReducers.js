import { BENEFICIARIES_DATA_FAIL, BENEFICIARIES_DATA_REQUEST, BENEFICIARIES_DATA_RESET, BENEFICIARIES_DATA_SUCCESS, CONFIRM_OTP_FAIL, CONFIRM_OTP_REQUEST, CONFIRM_OTP_RESET, CONFIRM_OTP_SUCCESS, SEND_OTP_FAIL, SEND_OTP_REQUEST, SEND_OTP_RESET, SEND_OTP_SUCCESS } from "../constants/userConstants";

export const userSendOTPReducer = (state = { number: '', txnId: '' }, action) => {
  switch (action.type) {
    case SEND_OTP_REQUEST:
      return { ...state, loading: true, error: '', number: action.payload };
    case SEND_OTP_SUCCESS:
      return { ...state, loading: false, success: true, txnId: action.payload };
    case SEND_OTP_FAIL:
      return { ...state, loading: false, success: false, error: action.payload };
    case SEND_OTP_RESET:
      return { ...state, loading: false, success: false, error: '' };
    default:
      return state;
  }
};

export const userConfirmOTPReducer = (state = { number: '', token: '' }, action) => {
  switch (action.type) {
    case CONFIRM_OTP_REQUEST:
      return { ...state, loading: true, error: '', number: action.payload };
    case CONFIRM_OTP_SUCCESS:
      return { ...state, loading: false, success: true, token: action.payload };
    case CONFIRM_OTP_FAIL:
      return { ...state, loading: false, success: false, error: action.payload };
    case CONFIRM_OTP_RESET:
      return { ...state, loading: false, success: false, error: '' };
    default:
      return state;
  }
};

export const userBeneficiariesReducer = (state = { beneficiaries: [], loading: false, success: false, error: '' }, action) => {
  switch (action.type) {
    case BENEFICIARIES_DATA_REQUEST:
      return { ...state, loading: true, error: '' };
    case BENEFICIARIES_DATA_SUCCESS:
      return { ...state, loading: false, success: true, error: '', beneficiaries: action.payload };
    case BENEFICIARIES_DATA_FAIL:
      return { ...state, loading: false, success: false, beneficiaries: [], error: action.payload };
    case BENEFICIARIES_DATA_RESET:
      return { ...state, loading: false, success: false, beneficiaries: [], error: '' };
    default:
      return state;
  }
};