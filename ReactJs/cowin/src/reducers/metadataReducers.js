import { DISTRICT_LIST_SUCCESS, ID_TYPES_SUCCESS, STATE_LIST_SUCCESS, UPDATE_PREF_STATUS } from "../constants/metadataConstants";

export const metadataReducer = (state = { states: [], districts: [] }, action) => {
  switch (action.type) {
    case STATE_LIST_SUCCESS:
      return { ...state, states: action.payload };
    case DISTRICT_LIST_SUCCESS:
      return { ...state, districts: action.payload };
    case ID_TYPES_SUCCESS:
      return { ...state, idTypes: action.payload };
    case UPDATE_PREF_STATUS:
      return { ...state, preferenceStatus: action.payload };
    default:
      return state;
  }
};