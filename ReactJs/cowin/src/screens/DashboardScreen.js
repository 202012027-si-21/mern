import { MDBCol, MDBRow } from 'mdb-react-ui-kit'
import React, { useEffect } from 'react'
import { Button } from 'react-bootstrap'
import { InfoLg } from 'react-bootstrap-icons'
import { useSelector, useDispatch } from 'react-redux'
import { getIdTypes } from '../actions/metadataActions'
import { getBeneficiaries } from '../actions/userActions'
import Beneficiary from '../components/Beneficiary'
import Header from '../components/Header'
import Loader from '../components/Loader'
import MainHeader from '../components/MainHeader'
import RegistrationFooter from '../components/RegistrationFooter'

const DashboardScreen = ({ history }) => {
  const dispatch = useDispatch()
  const userBeneficiaries = useSelector((state) => state.userBeneficiaries);
  const { loading, success, error, beneficiaries } = userBeneficiaries;

  const userConfirmOTP = useSelector((state) => state.userConfirmOTP);
  const { number } = userConfirmOTP;

  useEffect(() => {
    dispatch(getIdTypes())
  }, [dispatch])

  useEffect(() => {
    if (!loading && !success && error.length === 0) {
      dispatch(getBeneficiaries())
    } else if (!loading && !success && error.length !== 0) {
      history.push('/selfregistration')
    }
  })

  return (
    <div className='d-flex flex-column' id='registration__container'>
      {loading && <Loader></Loader>}
      <div className='registration__header'>
        <Header></Header>
        <MainHeader></MainHeader>
      </div>
      <MDBRow className='d-flex justify-content-center mb-3' id='dashboard__container'>
        <MDBCol className='mt-5 dashboard-beneficiaries__container' xs={12} lg={8}>
          <MDBRow className='px-3'>
            <MDBCol xs={12}><h3 className='dashboard-beneficiaries__header'>Account Details</h3></MDBCol>
            <MDBCol xs={12} md={6} className='d-flex align-items-center'>
              <p className='dashboard-beneficiaries__mobile'>Registered Mobile Number: <span>XXXX-XXXX-{`${number[6]}${number[7]}${number[8]}${number[9]}`}</span></p>
            </MDBCol>
            <MDBCol xs={12} md={6}>
              <MDBRow>
                <MDBCol xs={12} lg={6}></MDBCol>
                <MDBCol xs={12} lg={6}>
                  <Button className='dashboard-beneficiaries__issue' variant="outline-secondary">Raise an Issue</Button>
                </MDBCol>
              </MDBRow>
            </MDBCol>
          </MDBRow>
          {beneficiaries.map(beneficiary => {
            return <Beneficiary key={beneficiary.beneficiary_reference_id} beneficiary={beneficiary} />
          })}
          <MDBRow>
            <MDBCol xs={12} className='mb-4 mt-1 d-flex justify-content-center'>
              <div className="book-btn">
                <Button className='px-3' variant="outline-secondary">+ Add Member</Button>
              </div>
            </MDBCol>
          </MDBRow>

          <MDBRow className='blue-box'>
            <MDBCol xs={12} className='p-0'>
              <div className="blue-box-content">
                <h5><InfoLg width={20} height={15}></InfoLg> Note</h5>
                <ul><li>One registration per person is sufficient. Please do not register with multiple mobile numbers or different Photo ID Proofs for same individual.</li><li>Scheduling of Second dose should be done from the same account (same mobile number) from which the first dose has been taken, for generation of final certificate. Separate registration for second dose is not necessary.</li><li>Please carry the registered mobile phone and the requisite documents, including appointment slip, the Photo ID card used for registration, Employment Certificate (HCW/FLW) etc., while visiting the vaccination center, for verification at the time of vaccination.</li><li>Please check for additional eligibility conditions, if any, prescribed by the respective State/UT Government for vaccination at Government Vaccination Centers, for 18-44 age group, and carry the other prescribed documents (e.g. Comorbidity Certificate etc.) as suggested by respective State/UT (on their website).</li><li>The slots availability is displayed in the search (on district, pincode or map) based on the schedule populated by the DIOs (for Government Vaccination Centers) and private hospitals for their vaccination centers.</li><li>The vaccination schedule published by DIOs and private hospitals displays the list of vaccination centers with the following information <ul><li>The vaccine type.</li><li>The age group (18-44/45+ etc.).</li><li>The number of slots available for dose 1 and dose 2.</li><li>Whether the service is Free or Paid (Vaccination is free of cost at all the Government Vaccination Centers).</li><li>Per dose price charged by a private hospital.</li></ul></li><li>If you are seeking 1st dose vaccination, the system will show you only the available slots for dose 1. Similarly, if you are due for 2nd dose, the system will show you the available slots for dose 2 after the minimum period from the date of 1st dose vaccination has elapsed.</li><li>Once a session has been published by the DIO/ private hospital, the session now can not be cancelled. However, the session may be rescheduled. In case you have booked an appointment in any such vaccination session that is rescheduled for any reason, your appointment will also be automatically rescheduled accordingly. You will receive a confirmation SMS in this regard. On such rescheduling, you would still have the option of cancelling or further rescheduling such appointment.</li><li>When deleting any member from your account, please note they will lose access to their online vaccination certificate.</li></ul>
              </div>
            </MDBCol>
          </MDBRow>
        </MDBCol>
      </MDBRow>
      <RegistrationFooter></RegistrationFooter>
    </div>
  )
}

export default DashboardScreen
