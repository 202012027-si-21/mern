import { UPDATE_FILTERS } from '../constants/availabilityConstants'

const filterAgeSession = ({ session, is18Plus, is1844, is45Plus, hasCovaxin, hasCovishield, hasSputnik }) => {
  if ((is18Plus && session.allow_all_age) || (is1844 && session.min_age_limit === 18 && session.max_age_limit === 44) || (is45Plus && session.min_age_limit === 45)) {
    return true
  } else {
    return false
  }
}

const filterVaccineSession = ({ session, hasCovaxin, hasCovishield, hasSputnik }) => {
  if ((hasCovishield && session.vaccine === 'COVISHIELD') || (hasCovaxin && session.vaccine === 'COVAXIN') || (hasSputnik && session.vaccine === 'SPUTNIK V')) {
    return true
  } else {
    return false
  }
}

const ageFiltersFn = ({ center, isAgeAll, is18Plus, is1844, is45Plus }) => {
  const n_center = { ...center }
  if (is18Plus && is1844 && is45Plus) {
    return n_center;
  } else if (!is18Plus && !is1844 && !is45Plus) {
    return n_center;
  } else {
    const sessions = center.sessions;
    const newSessions = sessions.filter(session => filterAgeSession({ session, isAgeAll, is18Plus, is1844, is45Plus }))
    if (newSessions.length > 0) {
      n_center.sessions = newSessions
      return n_center
    } else {
      return false
    }
  }
}

const vaccineFiltersFn = ({ center, hasAllVaccine, hasCovaxin, hasCovishield, hasSputnik }) => {
  const n_center = { ...center }
  if (hasCovaxin && hasCovishield && hasSputnik) {
    return n_center;
  } else if (!hasCovaxin && !hasCovishield && !hasSputnik) {
    return n_center;
  } else {
    const sessions = center.sessions;
    const newSessions = sessions.filter(session => filterVaccineSession({ session, hasAllVaccine, hasCovaxin, hasCovishield, hasSputnik }))
    if (newSessions.length > 0) {
      n_center.sessions = newSessions
      return n_center
    } else {
      return false
    }
  }
}

export const updateFilters = (filterType, filter) => async (dispatch, getState) => {

  // const {
  //   availabilityFilters: { filters }
  // } = getState();

  const {
    availabilityList: { centers, filters }
  } = getState();

  const newFiltersState = {
    ...filters,
    [filterType]: {
      ...filters[filterType],
      [filter]: {
        ...filters[filterType][filter],
        selected: !filters[filterType][filter]['selected']
      }
    }
  }

  const ageFilters = newFiltersState.age
  const vaccineFilters = newFiltersState.vaccine
  const costFilters = newFiltersState.cost

  const isAgeAll = (ageFilters['18-plus']['selected'] === ageFilters['18-44']['selected']) && (ageFilters['18-plus']['selected'] === ageFilters['45-plus']['selected'])

  const hasAllVaccine = (vaccineFilters['covishield']['selected'] === vaccineFilters['covaxin']['selected']) && (vaccineFilters['covishield']['selected'] === vaccineFilters['sputnik']['selected'])

  const allCost = costFilters['paid']['selected'] === costFilters['free']['selected']

  const filteredCentersByAge = []
  centers.forEach(center => {
    const newCenter = ageFiltersFn({ center, is18Plus: ageFilters['18-plus']['selected'], is1844: ageFilters['18-44']['selected'], is45Plus: ageFilters['45-plus']['selected'], isAgeAll })
    if (newCenter) {
      filteredCentersByAge.push(newCenter)
    }
  })


  const filteredCentersByVaccine = []
  filteredCentersByAge.forEach(center => {
    const newCenter = vaccineFiltersFn({ center, hasAllVaccine, hasCovaxin: vaccineFilters['covaxin']['selected'], hasCovishield: vaccineFilters['covishield']['selected'], hasSputnik: vaccineFilters['sputnik']['selected'] })
    if (newCenter) {
      filteredCentersByVaccine.push(newCenter)
    }
  })

  let filteredCentersByCost = []
  if (!allCost) {
    filteredCentersByCost = filteredCentersByVaccine.filter(center => {
      return (costFilters['paid']['selected'] && center.fee_type === 'Paid') || (costFilters['free']['selected'] && center.fee_type === 'Free')
    })
  } else {
    filteredCentersByCost = filteredCentersByVaccine
  }


  dispatch({
    type: UPDATE_FILTERS,
    payload: { filters: newFiltersState, filteredCenters: filteredCentersByCost },
  });
};

export const updateFilteredData = (centers, filters) => {

  // const {
  //   availabilityFilters: { filters }
  // } = getState();

  const ageFilters = filters.age
  const vaccineFilters = filters.vaccine
  const costFilters = filters.cost

  const isAgeAll = (ageFilters['18-plus']['selected'] === ageFilters['18-44']['selected']) && (ageFilters['18-plus']['selected'] === ageFilters['45-plus']['selected'])

  const hasAllVaccine = (vaccineFilters['covishield']['selected'] === vaccineFilters['covaxin']['selected']) && (vaccineFilters['covishield']['selected'] === vaccineFilters['sputnik']['selected'])

  const allCost = costFilters['paid']['selected'] === costFilters['free']['selected']

  const filteredCentersByAge = []
  centers.forEach(center => {
    const newCenter = ageFiltersFn({ center, is18Plus: ageFilters['18-plus']['selected'], is1844: ageFilters['18-44']['selected'], is45Plus: ageFilters['45-plus']['selected'], isAgeAll })
    if (newCenter) {
      filteredCentersByAge.push(newCenter)
    }
  })


  const filteredCentersByVaccine = []
  filteredCentersByAge.forEach(center => {
    const newCenter = vaccineFiltersFn({ center, hasAllVaccine, hasCovaxin: vaccineFilters['covaxin']['selected'], hasCovishield: vaccineFilters['covishield']['selected'], hasSputnik: vaccineFilters['sputnik']['selected'] })
    if (newCenter) {
      filteredCentersByVaccine.push(newCenter)
    }
  })

  let filteredCentersByCost = []
  if (!allCost) {
    filteredCentersByCost = filteredCentersByVaccine.filter(center => {
      return (costFilters['paid']['selected'] && center.fee_type === 'Paid') || (costFilters['free']['selected'] && center.fee_type === 'Free')
    })
  } else {
    filteredCentersByCost = filteredCentersByVaccine
  }


  return filteredCentersByCost
};