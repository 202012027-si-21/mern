import axios from 'axios'
import { PUBLIC_REPORTS_SUCCESS } from '../constants/publicReportsConstants'

export const getPublicReports = (date) => async (dispatch) => {
  try {
    const { data } = await axios(`https://cdn-api.co-vin.in/api/v1/reports/v2/getPublicReports?state_id=&district_id=&date=`)
    dispatch({
      type: PUBLIC_REPORTS_SUCCESS,
      payload: data
    })

  } catch (error) {
    console.log(error)
  }
}