import React, { useState } from "react";
import reactDom from "react-dom";

import Button from "../../UI/Button/Button";
import styles from "./CourseInput.module.css";


const FormHtml = props => {
  return(
    <form onSubmit={props.formSubmitHandler}>
      <div className={`${styles['form-control']} + " " + ${!props.isValid ? styles['invalid'] : ''}  `}>
        <label>Course Goal</label>
        <input
          type="text"
          onChange={props.goalInputChangeHandler}
          value={props.enteredValue}
        />
      </div>
      <Button type="submit">Add Goal</Button>
  </form>
  )
}

const CourseInput = (props) => {
  const [enteredValue, setEnteredValue] = useState("");
  const [isValid, setIsValid] = useState(true);

  const goalInputChangeHandler = (event) => {
    if (event.target.value.trim().length > 0) {
      setIsValid(true)
    }
    setEnteredValue(event.target.value);
  };
  
  const formSubmitHandler = (event) => {
    event.preventDefault();
    if (enteredValue.trim().length === 0) {
      setIsValid(false);
      return;
    }
  
    props.onAddGoal(enteredValue);
    setEnteredValue("");
  };

  return (
    <React.Fragment>
    {reactDom.createPortal(
      <FormHtml
        formSubmitHandler={formSubmitHandler}
        isValid={isValid}
        goalInputChangeHandler={goalInputChangeHandler}
        enteredValue={enteredValue}
      />,
      document.getElementById('goal-form')
    )}
    </React.Fragment>
  );
};

export default CourseInput;
