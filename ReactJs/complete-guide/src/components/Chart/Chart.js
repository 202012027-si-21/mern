import ChartBar from './ChartBar'

import './Chart.css'

const Chart = props => {
    const valueArr = props.points.map(dp => dp.value)
    const totalMax = Math.max(...valueArr)

    return <div className="chart">
        {props.points.map(point =>
            <ChartBar
                key={point.label}
                value={point.value}
                maxValue={totalMax}
                label={point.label}
            />)}
    </div>
}

export default Chart