import React, { useState } from 'react'

import ExpenseForm from './ExpenseForm'
import Card from '../UI/Card'

import './NewExpense.css'

const NewExpense = (props) => {
    const [isEditing, setIsEditing] = useState(false);

    const onSaveExpenseDataHandler = (expenseData) => {
        expenseData.id = Math.random().toString()
        props.addExpenseData(expenseData)
        setIsEditing(false)
    }

    const cancelEditingHandler = () => {
        setIsEditing(false)
    }

    const startEditingHandler = () => {
        setIsEditing(true)
    }

    return (
        <Card className="new-expense">
            {!isEditing && <button onClick={startEditingHandler}>Add New Expense</button>}
            {isEditing && (
                <ExpenseForm
                    onSaveExpenseData={onSaveExpenseDataHandler}
                    onCancelEditing={cancelEditingHandler}
                />
            )}
        </Card>

    )
}

export default NewExpense