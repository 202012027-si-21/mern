import HeaderCartButton from "./HeaderCartButton";

import styles from "./Header.module.css";
import mealsImg from "../../assets/meals.jpg";

const Header = (props) => {
    return (
        <>
            <header className={styles.header}>
                <h1>Meals of Rajkot</h1>
                <HeaderCartButton onClick={props.onShowCart} />
            </header>
            <div className={styles["main-image"]}>
                <img src={mealsImg} alt="Delicious food!"></img>
            </div>
        </>
    );
};

export default Header;
