import { useContext, useState } from "react";

import Modal from "../UI/Modal/Modal";
import CartItem from "./CartItem";
import Checkout from "./Checkout";

import CartContext from "../../store/cart-context";
import styles from "./Cart.module.css";

const Cart = (props) => {
    const [isChekout, setIsCheckout] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [didSubmit, setDidSubmit] = useState(false);

    const context = useContext(CartContext);

    const totalAmount = `${context.totalAmount.toFixed(2)}`;
    const hasItems = context.items.length > 0;

    const cartItemRemoveHandler = (id) => {
        context.removeItem(id);
    };
    const cartItemAddHandler = (item) => {
        context.addItem({ ...item, amount: 1 });
    };

    const orderHandler = () => {
        setIsCheckout(true);
    };

    const submitOrderHandler = async (userData) => {
        setIsSubmitting(true);
        await fetch("https://health-care-bac26.firebaseio.com/orders.json", {
            method: "POST",
            body: JSON.stringify({
                user: userData,
                orderItems: context.items,
            }),
        });

        setIsSubmitting(false);
        setDidSubmit(true);
        context.clearCart();
    };

    const cartItems = (
        <ul className={styles["cart-items"]}>
            {context.items.map((item) => (
                <CartItem
                    key={item.id}
                    name={item.name}
                    amount={item.amount}
                    price={item.price}
                    onRemove={cartItemRemoveHandler.bind(null, item.id)}
                    onAdd={cartItemAddHandler.bind(null, item)}
                />
            ))}
        </ul>
    );

    const modalActions = (
        <div className={styles.actions}>
            <button className={styles["button--alt"]} onClick={props.onClose}>
                Close
            </button>
            {hasItems && (
                <button className={styles.button} onClick={orderHandler}>
                    Order
                </button>
            )}
        </div>
    );

    const cartModalContent = (
        <>
            {cartItems}
            <div className={styles.total}>
                <span>Total Amount</span>
                <span>&#8377;{totalAmount}</span>
            </div>
            {isChekout && (
                <Checkout
                    onSubmit={submitOrderHandler}
                    onCancel={props.onClose}
                />
            )}
            {!isChekout && modalActions}
        </>
    );

    const isSubmittingModalContent = <p>Sending order data...</p>;
    const didSubmitModalContent = (
        <>
            <p>Successfully sent the order!</p>
            <div className={styles.actions}>
                <button className={styles["button"]} onClick={props.onClose}>
                    Close
                </button>
            </div>
        </>
    );

    return (
        <Modal onClose={props.onClose}>
            {!isSubmitting && !didSubmit && cartModalContent}
            {isSubmitting && isSubmittingModalContent}
            {!isSubmitting && didSubmit && didSubmitModalContent}
        </Modal>
    );
};

export default Cart;
