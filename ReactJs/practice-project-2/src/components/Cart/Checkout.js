import { useRef, useState } from "react";

import styles from "./Checkout.module.css";

const isEmpty = (value) => {
    return value.trim() === "";
};
const isNotFiveChars = (value) => {
    return value.trim().length !== 6;
};

const Checkout = (props) => {
    const [formInputValidity, setformInputValidity] = useState({
        name: true,
        street: true,
        postal: true,
    });

    const nameInputRef = useRef();
    const streetInputRef = useRef();
    const postalInputRef = useRef();

    const submitHandler = (event) => {
        event.preventDefault();
        const enteredName = nameInputRef.current.value;
        const enteredStreet = streetInputRef.current.value;
        const enteredPostal = postalInputRef.current.value;

        const enteredNameIsValid = !isEmpty(enteredName);
        const enteredStreetIsValid = !isEmpty(enteredStreet);
        const enteredPostalIsValid = !isNotFiveChars(enteredPostal);

        setformInputValidity({
            name: enteredNameIsValid,
            street: enteredStreetIsValid,
            postal: enteredPostalIsValid,
        });

        const formIsValid =
            enteredNameIsValid && enteredStreetIsValid && enteredPostalIsValid;

        if (!formIsValid) {
            return;
        }

        props.onSubmit({
            name: enteredName,
            street: enteredStreet,
            postal: enteredPostal,
        });
    };

    const nameControlClasses = `${styles.control} ${
        !formInputValidity.name ? styles.invalid : ""
    }`;

    const streetControlClasses = `${styles.control} ${
        !formInputValidity.street ? styles.invalid : ""
    }`;

    const postalControlClasses = `${styles.control} ${
        !formInputValidity.postal ? styles.invalid : ""
    }`;

    return (
        <form onSubmit={submitHandler}>
            <div className={nameControlClasses}>
                <label htmlFor="name">Name</label>
                <input type="text" id="name" ref={nameInputRef} />
                {!formInputValidity.name && <p>Please enter a valid name.</p>}
            </div>
            <div className={streetControlClasses}>
                <label htmlFor="street">Street</label>
                <input type="text" id="street" ref={streetInputRef} />
                {!formInputValidity.street && (
                    <p>Please enter a valid street.</p>
                )}
            </div>
            <div className={postalControlClasses}>
                <label htmlFor="postal">Zip Code</label>
                <input type="text" id="postal" ref={postalInputRef} />
                {!formInputValidity.postal && (
                    <p>Please enter a valid zip code.</p>
                )}
            </div>

            <div className={styles.actions}>
                <button type="button" onClick={props.onCancel}>
                    Cancel
                </button>
                <button>Confirm</button>
            </div>
        </form>
    );
};

export default Checkout;
