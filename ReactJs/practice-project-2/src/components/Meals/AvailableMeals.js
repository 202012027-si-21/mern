import { useEffect, useState } from "react";

import Card from "../UI/Card/Card";

import styles from "./AvailableMeals.module.css";
import MealItem from "./MealItem/MealItem";

const AvailableMeals = () => {
    const [meals, setMeals] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [httpError, setHttpError] = useState(null);

    useEffect(() => {
        const getMealsData = async () => {
            const res = await fetch(
                "https://health-care-bac26.firebaseio.com/meals.json"
            );

            if (!res.ok) {
                throw new Error("Something went wrong!");
            }

            const mealsData = await res.json();

            const loadedMeals = [];

            for (const key in mealsData) {
                loadedMeals.push({
                    id: key,
                    name: mealsData[key].name,
                    description: mealsData[key].description,
                    price: mealsData[key].price,
                });
            }

            setMeals(loadedMeals);
            setIsLoading(false);
        };

        getMealsData().catch((err) => {
            setIsLoading(false);
            setHttpError(err.message);
        });
    }, []);

    if (isLoading) {
        return (
            <section className={styles.mealsLoading}>
                <p>Loading...</p>
            </section>
        );
    }

    if (httpError) {
        return (
            <section className={styles.mealsError}>
                <p>{httpError}</p>
            </section>
        );
    }

    const mealslist = meals.map((meal) => {
        return (
            <MealItem
                key={meal.id}
                id={meal.id}
                name={meal.name}
                description={meal.description}
                price={meal.price}
            />
        );
    });

    return (
        <section className={styles.meals}>
            <Card>
                <ul>{mealslist}</ul>
            </Card>
        </section>
    );
};

export default AvailableMeals;
