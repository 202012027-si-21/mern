import { useReducer } from "react";

import CartContext from "./cart-context";

const defaultCartState = {
    items: [],
    totalAmount: 0,
};

const cartReducer = (state, action) => {
    if (action.type === "ADD") {
        const updatedTotalAmount =
            state.totalAmount + action.item.price * action.item.amount;

        const existingItemIndex = state.items.findIndex((item) => {
            return item.id === action.item.id;
        });

        const existingCartItem = state.items[existingItemIndex];
        let updatedItems;

        if (existingCartItem) {
            const updatedItem = {
                ...existingCartItem,
                amount: existingCartItem.amount + action.item.amount,
            };

            updatedItems = [...state.items];
            updatedItems[existingItemIndex] = updatedItem;
        } else {
            updatedItems = state.items.concat(action.item);
        }

        return {
            items: updatedItems,
            totalAmount: updatedTotalAmount,
        };
    }
    if (action.type === "REMOVE") {
        const existingItemIndex = state.items.findIndex((item) => {
            return item.id === action.id;
        });

        const existingCartItem = state.items[existingItemIndex];
        let updatedItems;

        const updatedTotalAmount = state.totalAmount - existingCartItem.price;

        if (existingCartItem.amount === 1) {
            updatedItems = state.items.filter((item) => item.id !== action.id);
        } else {
            const updatedItem = {
                ...existingCartItem,
                amount: existingCartItem.amount - 1,
            };
            updatedItems = [...state.items];
            updatedItems[existingItemIndex] = updatedItem;
        }

        return {
            items: updatedItems,
            totalAmount: updatedTotalAmount,
        };
    } if(action.type === 'CLEAR'){
        return defaultCartState
    }

    return defaultCartState;
};

const CartProvider = (props) => {
    const [cartState, cartDispatch] = useReducer(cartReducer, defaultCartState);

    const addItemHandler = (item) => {
        cartDispatch({ type: "ADD", item: item });
    };

    const removeItemHandler = (id) => {
        cartDispatch({ type: "REMOVE", id: id });
    };

    const clearCartHandler = (id) => {
        cartDispatch({ type: "CLEAR"});
    };

    const cartContext = {
        items: cartState.items,
        totalAmount: cartState.totalAmount,
        addItem: addItemHandler,
        removeItem: removeItemHandler,
        clearCart: clearCartHandler
    };

    return (
        <CartContext.Provider value={cartContext}>
            {props.children}
        </CartContext.Provider>
    );
};

export default CartProvider;
