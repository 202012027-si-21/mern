<div align="center">
  <img src="https://camo.githubusercontent.com/337c4e99eb112ec7e52d91f816b239a9381a5685ecad2e6b142a69e73e463d7e/68747470733a2f2f692e6962622e636f2f543142327066352f4d65726e2d6c6f676f2d7265706f2e706e67" title="MERN logo">
</div>

# Learning MERN

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)
<br><br>
_The MERN stack consists of MongoDB, Express, React / Redux, and Node.js. Given the popularity of React on the frontend and of Node.js on the backend, the MERN stack is one of the most popular stack of technologies for building a modern single-page app._
<br><br>This repository will consists of projects I will make throughout my learning of _MERN stack_ which will include projects for individual technologies `(ReactJs and NodeJs)` and combined MERN stack projects too.

## [ReactJs Projects](ReactJs/)

-   [complete-guide](ReactJs/complete-guide)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To learn the basics, components and states in ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/019db39b-f1b4-4375-ba5f-b07b1a5766ae?style=flat-square)](https://expense-tracker-guide.netlify.app/)

-   [styling-components-guide](ReactJs/styling-components)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To learn the styling in ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/8db5b0df-d4df-4f07-b7b7-4634106f0ccc?style=flat-square)](https://blissful-edison-d77e96.netlify.app/)

-   [practice-project](ReactJs/practice-project-1)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To practice the concepts learned which are mentioned above in ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/f49ef152-b8fb-4962-a7c6-8474320be5a8?style=flat-square)](https://reactjs-practice-project-1.netlify.app/)

-   [side-effects-guide](ReactJs/guide%20-%20side%20effects)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To learn the side effects concept in ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/c0ad0507-e7be-4db1-ab25-86135d56cd6f?style=flat-square)](https://react-side-effects-guide.netlify.app/)

-   [practice-project-2](ReactJs/practice-project-2)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To practice the concepts learned which are mentioned above in ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/9c01cbc7-15a9-4b89-984c-9813e5c22e0c?style=flat-square)](https://rajkot-food-app.netlify.app/)

-   [guide-http-requests](ReactJs/guide-http-requests)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To learn how to make http requests to firebase from ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/d1327e69-6fd7-4212-ae57-4563ec4f9bfc?style=flat-square)](https://reactjs-http-guide-firebase.netlify.app/)

-   [custom-hooks-guide](ReactJs/custom-hooks-guide)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To learn how to create useful custom hooks and use it in different components
    [Demo ![Netlify](https://img.shields.io/netlify/cb2514af-368c-421c-951c-63107c3f0ca7?style=flat-square)](https://react-guide-custom-hooks.netlify.app/)

-   [guide-redux](ReactJs/guide-redux)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To learn Redux and how to use it in ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/499ca79f-b25f-4555-b445-5b83add36304?style=flat-square)](https://reactjs-guide-redux.netlify.app/)

-   [react-mpa-guide](ReactJs/react-mpa-guide)
    [![Maintenance](https://img.shields.io/badge/Completed-No-red.svg)](https://shields.io/)
    \- To learn how to make multiple page application using react router in ReactJs
    [Demo ![Netlify](https://img.shields.io/netlify/499ca79f-b25f-4555-b445-5b83add36304?style=flat-square)](https://reactjs-mpa-guide.netlify.app/)
